import { faker } from "@faker-js/faker";

export const getNumeric = (
  length: number,
  options?: {
    min?: number;
    allowLeadingZeros?: boolean;
  }
) => {
  const min = options?.min ?? 0;
  if (min > 0) {
    return faker.datatype
      .number({
        min: min,
        max: Math.pow(10, length) - 1,
      })
      .toString();
  }

  return faker.random.numeric(length, options);
};
