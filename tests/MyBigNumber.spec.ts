import "mocha";
import { getLogger } from "../src/provider/LogConfig";
import integerTest from "./test/integer.spec";
import floatTest from "./test/float.spec";
import MyBigNumber from "../src/models/MyBigNumber";

describe("Sum bigger digit tests", () => {
  const logger = getLogger("test.modules.MyBigNumberTest");
  const testFunction = (randomNumber1: string, randomNumber2: string) => {
    logger.debug(`Two number test: ${randomNumber1}, ${randomNumber2} `);
    const result = MyBigNumber.sum(randomNumber1, randomNumber2, {
      ignoreLog: true,
    });
    return result;
  };
  integerTest(testFunction);
  floatTest(testFunction);
});
