import { assert } from "chai";
import { getNumeric } from "../utils/digits";

type TestFunction = (value1: string, value2: string) => string;

export default (testFunction: TestFunction) =>
  describe("Sum two number with each number is integer", () => {
    it("Sum of two zero", () => {
      const result = testFunction("0", "0");
      assert.equal(Number(result), 0);
    });

    it("Sum of two numbers of equal lengths", () => {
      const randomNumber1 = getNumeric(4);
      const randomNumber2 = getNumeric(4);
      const result = testFunction(randomNumber1, randomNumber2);
      assert.equal(
        Number(result),
        Number(randomNumber1) + Number(randomNumber2)
      );
    });

    it("Sum of two numbers of different lengths", () => {
      const randomNumber1 = getNumeric(5);
      const randomNumber2 = getNumeric(3);
      const result = testFunction(randomNumber1, randomNumber2);
      assert.equal(
        Number(result),
        Number(randomNumber1) + Number(randomNumber2)
      );
    });

    it("Sum of 2 2-digit numbers whose result is greater than hundreds", () => {
      const randomNumber1 = getNumeric(2, { min: 50 });
      const randomNumber2 = getNumeric(2, { min: 50 });
      const result = testFunction(randomNumber1, randomNumber2);
      assert.equal(
        Number(result),
        Number(randomNumber1) + Number(randomNumber2)
      );
    });

    it("Sum of 2 digit numbers, one of which is less than 2 digit and the result is more than hundreds", () => {
      const randomNumber1 = getNumeric(2, { min: 90 });
      const randomNumber2 = getNumeric(1);
      const result = testFunction(randomNumber1, randomNumber2);
      assert.equal(
        Number(result),
        Number(randomNumber1) + Number(randomNumber2)
      );
    });
  });
