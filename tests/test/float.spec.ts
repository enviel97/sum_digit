import { assert } from "chai";
import { getNumeric } from "../utils/digits";

type TestFunction = (value1: string, value2: string) => string;

export default (testFunction: TestFunction) =>
  describe("Sum two number with each number is float", () => {
    describe("Sum of two zero", () => {
      it("Two numbers is lest than 1", () => {
        const randomNumber1 = `${0}.${getNumeric(2)}`;
        const randomNumber2 = `${0}.${getNumeric(3)}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(3)
        );
      });

      it("Two numbers is 0", () => {
        const result = testFunction("0.00", "0.0");
        assert.equal(Number(result), 0);
      });
    });

    describe("Sum of two numbers of same lengths", () => {
      it("Two numbers with the same float part length", () => {
        const randomNumber1 = `${getNumeric(2)}.${getNumeric(2)}`;
        const randomNumber2 = `${getNumeric(2)}.${getNumeric(2)}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(2)
        );
      });
      it("Two numbers with different float part length", () => {
        const randomNumber1 = `${getNumeric(2)}.${getNumeric(3)}`;
        const randomNumber2 = `${getNumeric(2)}.${getNumeric(4)}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(4)
        );
      });
      it("Two numbers whose float part sum is greater than one hundred", () => {
        const randomNumber1 = `${getNumeric(2)}.${getNumeric(2, { min: 50 })}`;
        const randomNumber2 = `${getNumeric(2)}.${getNumeric(2, { min: 50 })}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(2)
        );
      });
    });

    describe("Sum of two numbers of different lengths", () => {
      it("Two numbers with the same float part length", () => {
        const randomNumber1 = `${getNumeric(3)}.${getNumeric(2)}`;
        const randomNumber2 = `${getNumeric(2)}.${getNumeric(2)}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(2)
        );
      });
      it("Two numbers with different float part length", () => {
        const randomNumber1 = `${getNumeric(4)}.${getNumeric(3)}`;
        const randomNumber2 = `${0}.${getNumeric(4)}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(4)
        );
      });
      it("Two numbers whose float part sum is greater than one hundred", () => {
        const randomNumber1 = `${getNumeric(1)}.${getNumeric(2, { min: 50 })}`;
        const randomNumber2 = `${getNumeric(2)}.${getNumeric(2, { min: 50 })}`;
        const result = testFunction(randomNumber1, randomNumber2);
        assert.equal(
          result,
          (Number(randomNumber1) + Number(randomNumber2)).toFixed(2)
        );
      });
    });
  });
