# SUM TWO DIGIT

## Getting start

This just provider a modules sum two numeric with allow decimal (format: xy.z) and integer using typescript

## Hardware, platform, special libraries used

- os (author): window 10 - powershell
- editor: vs code

- node: v19.1.0
- npm: v8.19.3

- mocha: v10.2.0
- chai: v4.3.7
- typescript: v4.9.5

## Run

Run on command (Terminal/powershell/...):

- npm i (Just first run)
- Some Script:
  - npm start: run normal app
  - npm run build: build from typescript to javascript
  - npm run test: run mocha test
  - npm run start:dev: run the app in developer mode

## Article

- [Setup typescript](https://dev.to/ozanbolel/installing-typescript-and-setting-up-your-development-environment-on-node-14lk)
- [Setup Mocha test](https://mochajs.org/)
