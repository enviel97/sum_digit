/**
 *
 * @param digits1 : numeric string
 * @param digits2 : numeric string
 * @returns object {
 *   big: string,
 *   small: string
 * }
 *
 */

export const getBigSmallDependLength = (digits1: string, digits2: string) => {
  let big = digits1;
  let small = digits2;
  if (big.length < small.length) {
    big = digits2;
    small = digits1;
  }

  return { big, small };
};
