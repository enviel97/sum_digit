class Digits {
  private computationSpaceQuantity: number;
  private computationSpace: string[];

  constructor(value: string) {
    this.computationSpace = value.split("");
    this.computationSpaceQuantity = value.length;
  }

  getDigits() {
    return Number(this.computationSpace.join(""));
  }

  getQuantity() {
    return this.computationSpaceQuantity;
  }

  getDigitAt(index: number): number {
    if (index >= this.computationSpaceQuantity) return 0;
    const value = Number(this.computationSpace[index]);
    if (Number.isNaN(value)) throw new Error("This not a digit");
    return value;
  }
}

export default Digits;
