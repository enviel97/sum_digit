import { getLogger } from "../provider/LogConfig";
import { getBigSmallDependLength } from "../utils/digit";
import Digits from "./Digits";

interface MyBigNumberSumProps {
  ignoreLog?: boolean;
}
class MyBigNumber {
  private static log = getLogger("modules.MyBigNumber");
  private static logIgnore: boolean = false;

  private static logHandler(
    first: Digits,
    secondary: Digits,
    point: number,
    sum: any,
    memory: any
  ) {
    const phrase = Math.abs(first.getQuantity() - point);
    this.log.info(`Phrase::: ${phrase}`);
    this.log.info(
      `Sum atom::: ${first.getDigitAt(point)} + ${secondary.getDigitAt(point)}`
    );
    this.log.info(`Current Sum result::: ${sum}`);
    this.log.info(`Memory::: ${memory}`);
    this.log.info(`--------- End: ${phrase} ---------\n`);
  }

  private static balanceDigitLength(
    stn1: string,
    stn2: string,
    part: "int" | "float"
  ) {
    let first = stn1;
    let secondary = stn2;
    // balance length
    if (first.length !== secondary.length) {
      const { big, small } = getBigSmallDependLength(first, secondary);
      first = big;
      secondary = small;
      const regex = part === "int" ? /^/ : /$/;
      while (secondary.length < first.length) {
        secondary = secondary.replace(regex, "0");
      }
    }

    return {
      first: new Digits(first),
      secondary: new Digits(secondary),
    };
  }

  private static sumHandler(stn1: Digits, stn2: Digits) {
    let memory = 0;
    const sum: number[] = [];
    for (let i = stn1.getQuantity() - 1; i >= 0; --i) {
      const temp = stn1.getDigitAt(i) + stn2.getDigitAt(i) + memory;
      if (i === 0) {
        sum.unshift(temp);
        memory = 0;
      } else {
        sum.unshift(temp % 10);
        memory = Math.floor(temp / 10);
      }
      !this.logIgnore && this.logHandler(stn1, stn2, i, sum, memory);
    }
    return sum;
  }

  private static sumPartInt(stn1: string, stn2: string): string {
    !this.logIgnore && this.log.info("[Title part]: Sum int part");
    if (stn1 === "0") return stn2;
    if (stn2 === "0") return stn1;

    const { first, secondary } = this.balanceDigitLength(stn1, stn2, "int");
    const sum = this.sumHandler(first, secondary);
    return sum.join("");
  }

  private static sumPartFloat(stn1: string, stn2: string) {
    !this.logIgnore && this.log.info("[Title part]: Sum float part");
    if (stn1 === "0") return { sum: stn2, memory: 0 };
    if (stn2 === "0") return { sum: stn1, memory: 0 };

    const { first, secondary } = this.balanceDigitLength(stn1, stn2, "float");
    const sum = this.sumHandler(first, secondary);
    let memory = 0;
    if (sum[0] / 10 >= 1) {
      sum[0] = sum[0] % 10;
      memory = 1;
    }
    return { sum: sum.join(""), memory: memory };
  }

  public static sum(
    stn1: string,
    stn2: string,
    option?: MyBigNumberSumProps
  ): string {
    this.logIgnore = !!option?.ignoreLog;
    const [fistBeforeDot, fistAfterDot] = stn1.split(".");
    const [secondaryBeforeDot, secondaryAfterDot] = stn2.split(".");
    const resultBeforeDot = this.sumPartInt(fistBeforeDot, secondaryBeforeDot);
    if (!fistAfterDot && !secondaryAfterDot) {
      this.log.info(`Result sum::: ${stn1} + ${stn2} = ${resultBeforeDot}`);
      return resultBeforeDot;
    }

    // Calc
    const { sum: resultAfterDot, memory } = this.sumPartFloat(
      fistAfterDot ?? "0",
      secondaryAfterDot ?? "0"
    );

    let sum;
    if (memory !== 0) {
      sum = `${this.sumPartInt(resultBeforeDot, "1")}.${resultAfterDot}`;
    } else {
      sum = `${resultBeforeDot}.${resultAfterDot}`;
    }
    return sum;
  }
}

export default MyBigNumber;
